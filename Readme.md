# BomberMan

 This is the Bomberman programming Test made for IKA-Werke 

### Built With

 * UE4.17.2
 * Visual Studio Community 15

### Author

Tarek Karray


### License

 This project is licensed under the [MIT License](https://opensource.org/licenses/MIT)

